//
//  MockURLProtocol.swift
//  20230315-BenjaminBarkett-NYCSchoolsTests
//
//  Created by benjamin.barkett on 3/15/23.
//

import Foundation

class MockURLProtocol: URLProtocol {
    static var handler: ((URLRequest) throws -> (Data, HTTPURLResponse))?
    
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    override func startLoading() {
        guard let handler = MockURLProtocol.handler else { return }
        guard let urlProtocolClient = client else { return }
        
        do {
            let (data, response) = try handler(request)
            urlProtocolClient.urlProtocol(self, didLoad: data)
            urlProtocolClient.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
            urlProtocolClient.urlProtocolDidFinishLoading(self)
        } catch {
            urlProtocolClient.urlProtocol(self, didFailWithError: error)
        }
    }
    override func stopLoading() {
        //do nothing
    }
}
