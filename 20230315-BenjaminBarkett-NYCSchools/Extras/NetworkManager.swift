//
//  NetworkManager.swift
//  20230315-BenjaminBarkett-NYCSchools
//
//  Created by benjamin.barkett on 3/15/23.
//

import Foundation
import Combine

class NetworkManager {
    /*
     If I had more time I would've used URLComponents to build my URLs instead of using string literals.
     This would have allowed me to more gracefully handle any errors, as well as easily add options to allow the user to get a more specific list of schools
     based on parameters like location, size, etc. (the 2017 DOE High School Directory has over 400 columns, some of which could be used as SoQL select queries like in the schoolURLString).
     */
    private let session: URLSession
    
    private var schoolURLString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$select=dbn,school_name%20AS%20schoolName"
    private var satURLString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
    
    init(session: URLSession? = nil) {
        self.session = session ?? URLSession.shared
    }
    
    func fetchSchoolList() -> AnyPublisher<[School], Error> {
        let schoolURL = URL(string: schoolURLString)!
        return session.dataTaskPublisher(for: schoolURL)
            .tryMap { result in
                guard let urlResponse = result.response as? HTTPURLResponse, (200...299).contains(urlResponse.statusCode) else {
                    throw NetworkingError.invalidStatusCode
                }
                
                let schools = try JSONDecoder().decode([School].self, from: result.data)
                return schools
            }
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    func fetchSATData(for dbn: String) -> AnyPublisher<SATData, Error> {
        let satURL = URL(string: satURLString + dbn)!
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        return session.dataTaskPublisher(for: satURL)
            .tryMap { result in
                guard let urlResponse = result.response as? HTTPURLResponse, (200...299).contains(urlResponse.statusCode) else {
                    throw NetworkingError.invalidStatusCode
                }
                let satData = try decoder.decode([SATData].self, from: result.data)
                guard !satData.isEmpty else {
                    throw NetworkingError.unknown
                }
                return satData[0]
            }
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}
