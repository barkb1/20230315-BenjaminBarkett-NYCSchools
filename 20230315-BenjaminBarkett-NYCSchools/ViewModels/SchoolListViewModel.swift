//
//  SchoolListViewModel.swift
//  20230315-BenjaminBarkett-NYCSchools
//
//  Created by benjamin.barkett on 3/15/23.
//

import Foundation
import Combine

class SchoolListViewModel: ObservableObject {
    @Published var schools = [School]()
    @Published var searchText = ""
    @Published var isLoading = false
    @Published var hasEncounteredError = false
    @Published var errorDescription = ""
    
    var schoolSearchResults: [School] {
        searchText.isEmpty ? schools : schools.filter { $0.schoolName.localizedCaseInsensitiveContains(searchText) }
    }
    var cancellables = Set<AnyCancellable>()
    
    func fetchSchools() {
        NetworkManager().fetchSchoolList()
            .sink { completion in
                switch completion {
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self.hasEncounteredError = true
                            self.errorDescription = error.localizedDescription
                        }
                    case .finished:
                        DispatchQueue.main.async {
                            self.isLoading = false
                        }
                        break
                }
            } receiveValue: { schools in
                self.schools = schools.sorted { $0.schoolName < $1.schoolName }
            }
            .store(in: &cancellables)
    }
}
