//
//  SchoolDetailView.swift
//  20230315-BenjaminBarkett-NYCSchools
//
//  Created by benjamin.barkett on 3/15/23.
//

import SwiftUI
import Combine

struct SchoolDetailView: View {
    @State private var name = ""
    @State private var numOfTestTakers = ""
    @State private var mathScore = ""
    @State private var readingScore = ""
    @State private var writingScore = ""
    @State private var showingErrorAlert = false
    @State var errorDescription = ""
    @State private var cancellables = Set<AnyCancellable>()
    
    var dbn: String

    var body: some View {
        Form {
            Section("School") {
                Text("Name: \(name.capitalized)")
                Text("DBN: \(dbn)")
            }

            Section("SAT Info") {
                Text("Number of SAT Test Taker: \(numOfTestTakers)")
                Text("Average Math Score: \(mathScore)")
                Text("Average Critical Reading Score: \(readingScore)")
                Text("Average Writing Score: \(writingScore)")
            }
        }
        .navigationTitle("School Info")
        .onAppear {
            fetchSATData()
        }
        .alert("Error Loading School", isPresented: $showingErrorAlert) {
            Button {
                fetchSATData()
            } label: {
                Text("Retry")
            }
            Button(role: .cancel) {
            } label: {
                Text("Cancel")
            }
        } message: {
            Text(errorDescription)
        }
    }
    
    func fetchSATData() {
       NetworkManager().fetchSATData(for: dbn)
            .sink { completion in
                switch completion {
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self.showingErrorAlert = true
                            self.errorDescription = error.localizedDescription
                        }
                    case .finished:
                        break
                }
            } receiveValue: { data in
                self.name = data.schoolName
                self.numOfTestTakers = data.numOfSatTestTakers
                self.mathScore = data.satMathAvgScore
                self.readingScore = data.satCriticalReadingAvgScore
                self.writingScore = data.satWritingAvgScore
            }
            .store(in: &cancellables)
    }
}

/*
 I made the decision to make the network call as a function inside the view
 because this is a relatively simple use case and I prefer to do as little
 'heavy-lifting' as possible inside of SwiftUI views. I could have passed in
 the SchoolListViewModel that was constructed in SchoolListView and called a
 function inside there to grab the data, but that would've presented challenges
 like using an optional `currentSchool` property (or something similar) that
 would need to be nil-coalesced in this view lest I want to load the SAT data
 for all 470 schools on startup. Perhaps if the API was slower, or there was a
 lot of data to load per school, I could have implemented a more asynchronous
 solution. However, this just seemed easiest given the time constraint and app
 requirements.
 
 Below is an example of an implementation with a ViewModel injection:
 
 struct SchoolDetailView: View {
    @ObservedObject var viewModel: SchoolListViewModel
    var dbn: String
 
    var body: some View {
        Form {
            Section("School") {
                Text("Name: \(viewModel.currentSchool?.schoolName.capitalized ?? "")")
                Text("DBN: \(viewModel.currentSchool?.dbn ?? "")")
            }
 
            Section("SAT Info") {
                Text("Number of SAT Test Taker: \(viewModel.currentSchool?.numOfSatTestTakers ?? "")")
                Text("Average Math Score: \(viewModel.currentSchool?.satMathAvgScore ?? "")")
                Text("Average Critical Reading Score: \(viewModel.currentSchool?.satCriticalReadingAvgScore ?? "")")
                Text("Average Writing Score: \(viewModel.currentSchool?.satWritingAvgScore ?? "")")
            }
        }
        .navigationTitle("School Info")
        .onAppear {
            viewModel.fetchSATData(for: dbn)
        }
    }
 }
 */

struct SchoolDetailView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetailView(dbn: "02M260")
    }
}
