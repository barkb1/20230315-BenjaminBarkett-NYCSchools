//
//  SATData.swift
//  20230315-BenjaminBarkett-NYCSchools
//
//  Created by benjamin.barkett on 3/15/23.
//

import Foundation

struct SATData: Codable {
    var dbn: String
    var schoolName: String
    var numOfSatTestTakers: String
    var satCriticalReadingAvgScore: String
    var satMathAvgScore: String
    var satWritingAvgScore: String
}
