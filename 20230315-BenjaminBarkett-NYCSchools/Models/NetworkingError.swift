//
//  NetworkingError.swift
//  20230315-BenjaminBarkett-NYCSchools
//
//  Created by benjamin.barkett on 3/15/23.
//

import Foundation

public enum NetworkingError: Error {
    case invalidStatusCode
    case unknown
}

extension NetworkingError: CustomStringConvertible {
    public var description: String {
        switch self {
            case .invalidStatusCode:
                return "Invalid status code returned from server"
            case .unknown:
                return "An unknown error has occurred"
        }
    }
}
